package proyecto_3_201810_sec_4_team_7;

import java.io.BufferedReader;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.TestCase;
import model.data_structures.GrafoDirigido;
import model.data_structures.Iterador;
import model.vo.InfoArco;
import model.vo.Item;
import model.vo.VerticeItem;

public class TestDiGraf extends TestCase
{
	private String serviceFile = "./data/test/taxi-trips-wrvz-psew-subset-test.json";
	
	private GrafoDirigido<Double, VerticeItem, InfoArco> grafo;
	
	public void SetupEscenario1()
	{
		grafo = new GrafoDirigido<Double,VerticeItem,InfoArco>();
		try
		{
			BufferedReader bfr = new BufferedReader( new FileReader(""));
			fail("Debi� generar error");
		}
		catch(Exception e)
		{}
		try
		{
			BufferedReader bfr = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length; i++)
			{
				Item actual = item[i];
				if(grafo.darVertices() == null || actual.getPickup_centroid_latitude() == 0 || actual.getPickup_centroid_longitude() == 0
						|| actual.getDropoff_centroid_latitude() == 0 || actual.getDropoff_centroid_longitude() == 0)
				{
					continue;
				}
				Iterador<VerticeItem> lista = grafo.darVertices().darIterador();
				boolean termino = false;
				boolean termino2 = false;
				VerticeItem v1 = new VerticeItem(i, i, actual);
				VerticeItem v2 = new VerticeItem(i, i, actual);
				while(lista.haySiguiente()&& (!termino || !termino2))
				{
					VerticeItem verticeActual = lista.darSiguiente();	
					if(verticeActual.darId2(actual.getPickup_centroid_latitude(), actual.getPickup_centroid_longitude())<=25)
					{
						v1 = verticeActual;
						verticeActual.agregarItem(actual);
						termino = true;
					}
					if(verticeActual.darId2(actual.getDropoff_centroid_latitude(), actual.getDropoff_centroid_longitude())<= 25)
					{
						v2 = verticeActual;
						verticeActual.agregarItem(actual);
						termino2 = true;
					}
				}
				if(!termino)
				{
					v1 = new VerticeItem(actual.getPickup_centroid_latitude(), actual.getPickup_centroid_longitude(), actual);
					grafo.addVertex(v1.darId(), v1);
				}
				if(!termino2)
				{
					v2 = new VerticeItem(actual.getDropoff_centroid_latitude(), actual.getDropoff_centroid_longitude(), actual);
					grafo.addVertex(v2.darId(), v2);
				}
				try
				{
					InfoArco arco = (InfoArco) grafo.getInfoArc(v1.darId(), v2.darId()).darInfoArco();
					arco.aumentarCantTotal();
					arco.promediarPeso((int) (actual.getTrip_miles()*1609.34));
				}
				catch(Exception e)
				{
					InfoArco nuevoArco = new InfoArco(v1.darId(), v2.darId(), actual);
					grafo.addEdge(v1.darId(), v2.darId(), nuevoArco);
				}
			}
		}
		catch(Exception e) {
			fail("No debi� generar error");	
		}
	}
	
	public void testVertices()
	{
		SetupEscenario1();
		assertEquals("No era el resultado esperado", 20, grafo.V());
	}
	
	public void testArcos()
	{
		SetupEscenario1();
		assertEquals("No era el resultado esperado", 19, grafo.E());
	}
}
