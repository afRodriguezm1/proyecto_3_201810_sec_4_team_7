package model.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.data_structures.ArcoNoExisteException;
import model.data_structures.ArcoYaExisteException;
import model.data_structures.Camino;
import model.data_structures.Iterador;
import model.data_structures.Lista;
import model.data_structures.VerticeNoExisteException;
import model.data_structures.VerticeYaExisteException;
import model.logic.Manager;
import model.vo.InfoArco;
import model.vo.VerticeItem;

public class Controller 
{
	/**
	 * Manager de la estructura de datos.
	 */
	private static Manager manager = new Manager();

	/**
	 * Formato de fecha que se maneja en todo el proyecto
	 */
	private static SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public static void req0(String pDatos)
	{
		String[] partes = pDatos.split("-");
		String tamanio = (Integer.parseInt(partes[0]) ==1)? "small": (Integer.parseInt(partes[0]) ==2)? "medium": "large";
		int distancia = (Integer.parseInt(partes[1]) ==1)?100 : (Integer.parseInt(partes[1]) ==2)? 70 :(Integer.parseInt(partes[1]) ==3)? 50 : 25;
		String serviceFile = "./data/JsonFiles/grafo-" + tamanio +"-" + Integer.toString(distancia) +"-metros.json";
		manager.req0(serviceFile, distancia, tamanio);
	}
	
	public static VerticeItem req1()
	{
		VerticeItem respuesta = manager.req1();
		return respuesta;
	}
	
	public static double darDistancia()
	{
		return manager.darDistancia();
	}
	
	public static void req2()
	{
		manager.req2();
	}
	
	public static void req3()
	{
		manager.req3();
	}
	
	public static Camino<Double,VerticeItem,InfoArco> req4() throws VerticeNoExisteException, IOException
	{
		return manager.req4();
	}
	
	public static Camino<Double,VerticeItem,InfoArco>[] req5() throws IOException, VerticeNoExisteException
	{
		return manager.req5();
	}
	
	public static Lista[] req6() throws IOException, VerticeNoExisteException, ArcoNoExisteException
	{
		return manager.req6();
	}
	
}