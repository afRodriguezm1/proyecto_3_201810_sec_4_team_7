package model.vista;

import java.util.Date;
import java.util.Scanner;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import model.controller.Controller;
import model.data_structures.Camino;
import model.data_structures.Iterador;
import model.data_structures.Lista;
import model.vo.InfoArco;
import model.vo.Item;
import model.vo.VerticeItem;


public class Vista 
{
	
	/**
	 * M�todo principal de la aplicaci�n
	 * @param args
	 */
	public static void main(String[] args) 
	{
		try
		{
			Scanner sc = new Scanner(System.in);
			boolean fin=false;
			while(!fin)
			{
				printMenu();

				int option = sc.nextInt();
				String datos;

				long memoryBeforeCase1;
				long memoryAfterCase1;
				long startTime;
				long endTime;
				long duration;

				switch(option)
				{
				case 0:
					System.out.println("Escrba el tama�o del grafo y la distancia entre nodos separados por un guion (-)");
					System.out.println("-------CARGAR INFORMACI�N------\n1. -Datos de tama�o peque�o.\n2. -Datos de tama�o mediano.\n3. -Datos de tama�o grande.\n----------------------------");
					System.out.println("-----DISTANCIAS ENTRE NODOS-----\n1. -100 Metros.\n2. -70 Metros.\n3. -50 Metros.\n4. -25 Metros.\n----------------------------");
					
					datos = new Scanner(System.in).nextLine();

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					Controller.req0(datos);
					
					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);

					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");
					break;
					
				case 1:

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					VerticeItem req1 = Controller.req1();
					System.out.println("El vertice m�s congestionado tiene las siguientes caracter�sticas:");
					System.out.println("Latitud: "+ req1.darLatref());
					System.out.println("Longitud: "+ req1.longRef());
					int contadorEntra = 0;
					int contadorSale = 0;
					for(int i = 0; i < req1.darItems().darLongitud(); i++)
					{
						Item actual = req1.darItems().darElemento(i);
						double distancia = getDistance(actual.getPickup_centroid_latitude(), actual.getPickup_centroid_longitude(), req1.darLatref(), req1.longRef());
						double distancia2 = getDistance(actual.getDropoff_centroid_latitude(), actual.getDropoff_centroid_longitude(), req1.darLatref(), req1.longRef());
						if(distancia <= Controller.darDistancia())
						{
							contadorEntra++;
						}
						if(distancia2 <=Controller.darDistancia())
						{
							contadorSale++;
						}
					}
					System.out.println("Servicios que salieron: " + contadorEntra);
					System.out.println("Servicios que llegaron: " +  contadorSale);
					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);

					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");
					break;

				case 2:

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.nanoTime();

					Controller.req2();


					endTime = System.nanoTime();
					duration = (endTime - startTime);

					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " nanosegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000) + " MB\n--------------------------------------");

					break;

				case 3:

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					Controller.req3();
					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);
					
					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");
					break;

				case 4 :

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					Camino<Double,VerticeItem,InfoArco> caminoMasCorto = Controller.req4();
					if(caminoMasCorto == null)
					{
						System.out.println("No hay ning�n camino entre los dos vertices");
					}
					else
					{
						Iterador<VerticeItem> secuenciaVertices = caminoMasCorto.darSecuenciaVertices();
						System.out.println("La secuencia de vertices es:");
						int costo = 0;
						int tiempo = 0;
						int longitud = 0;
						while(secuenciaVertices.haySiguiente())
						{
							VerticeItem verActual = secuenciaVertices.darSiguiente();
							System.out.println("--->" + verActual);
						}
						Lista<InfoArco> arcos = caminoMasCorto.darSecuenciaArcos();
						for(int i = 0; i<arcos.darLongitud(); i++)
						{
							costo+=arcos.darElemento(i).darCosto();
							tiempo+= arcos.darElemento(i).darTiempo();
							longitud += arcos.darElemento(i).darPeso();
						}
						System.out.println("El camino tiene: "+ longitud + " metros");
						System.out.println("El camino toma: "+ tiempo + " segundos");
						System.out.println("El camino cuesta: "+ costo);
					}
					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);
					
					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");

					break;

				case 5:
					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					
					Camino<Double,VerticeItem,InfoArco>[] dobles = Controller.req5();
					Camino<Double,VerticeItem,InfoArco> caminoMasCortoIda = dobles[0];
					Camino<Double,VerticeItem,InfoArco> caminoMasCortoVuelta = dobles[1];
					if(caminoMasCortoIda==null)
					{
						System.out.println("--->Camino 1: No se encontro ningun camino de ida");
					}
					else
					{
						Iterador<VerticeItem> secuenciaVertices = caminoMasCortoIda.darSecuenciaVertices();
						System.out.println("---> Camino 1: ");
						int costo = 0;
						int tiempo = 0;
						int longitud = 0;
						while(secuenciaVertices.haySiguiente())
						{
							VerticeItem verActual = secuenciaVertices.darSiguiente();
							System.out.println("--->" + verActual);
						}
						Lista<InfoArco> arcos = caminoMasCortoIda.darSecuenciaArcos();
						for(int i = 0; i<arcos.darLongitud(); i++)
						{
							costo+=arcos.darElemento(i).darCosto();
							tiempo+= arcos.darElemento(i).darTiempo();
							longitud += arcos.darElemento(i).darPeso();
						}
						System.out.println("El camino tiene: "+ longitud + " metros");
						System.out.println("El camino toma: "+ tiempo + " segundos");
						System.out.println("El camino cuesta: "+ costo);
					}
					if(caminoMasCortoVuelta==null)
					{
						System.out.println("--->Camino 2: No se encontro ningun camino de Regreso");
					}
					else
					{
						Iterador<VerticeItem> secuenciaVertices = caminoMasCortoVuelta.darSecuenciaVertices();
						System.out.println("---> Camino 2: ");
						int costo = 0;
						int tiempo = 0;
						int longitud = 0;
						while(secuenciaVertices.haySiguiente())
						{
							VerticeItem verActual = secuenciaVertices.darSiguiente();
							System.out.println("--->" + verActual);
						}
						Lista<InfoArco> arcos = caminoMasCortoVuelta.darSecuenciaArcos();
						for(int i = 0; i<arcos.darLongitud(); i++)
						{
							costo+=arcos.darElemento(i).darCosto();
							tiempo+= arcos.darElemento(i).darTiempo();
							longitud += arcos.darElemento(i).darPeso();
						}
						System.out.println("El camino tiene: "+ longitud + " metros");
						System.out.println("El camino toma: "+ tiempo + " segundos");
						System.out.println("El camino cuesta: "+ costo);
					}

					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);
					
					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");

					break;

				case 6:

					memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					startTime = System.currentTimeMillis();

					Lista[] arreglo = Controller.req6();
					endTime = System.currentTimeMillis();
					duration = (endTime - startTime);
					Lista aux1 = arreglo[0];
					Lista aux2 = arreglo[1];
					if(aux1.darLongitud()==0)
						System.out.println("No se encontr� ningun camino sin peajes");
					else
					{
						System.out.println("Caminos ordenados crecientemente por tiempo:");
						for(int i = 0; i < aux1.darLongitud(); i++)
						{
							Camino camino = (Camino) aux1.darElemento(i);
							System.out.println("-> Camino:" + camino);
							Lista arcos = camino.darSecuenciaArcos();
							for(int j = 0; j < arcos.darLongitud(); j++)
							{
								System.out.println("  ->Id: " + (InfoArco)arcos.darElemento(j));
							}
						}
						System.out.println("Caminos ordenados decrecientemente por distancia:");
						for(int i = 0; i < aux2.darLongitud(); i++)
						{
							Camino camino = (Camino) aux2.darElemento(i);
							System.out.println("-> Camino:" + camino);
							Lista arcos = camino.darSecuenciaArcos();
							for(int j = 0; j < arcos.darLongitud(); j++)
							{
								System.out.println("  ->Id: " + (InfoArco)arcos.darElemento(j));
							}
						}
					}
					
					memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println("--------------------------------------\nTiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n--------------------------------------");
					break;

				case 7:	
					fin=true;
					sc.close();
					break;

				default: System.out.println("Opcion errada");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("\nError, opcion no v�lida, ingrese una opcion v�lida nuevamente\n");
			main(args);
		}
	}
	
	private static double toRad(double numero)
	{
		return Math.toRadians(numero);
	}
	
	public static double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
	 // TODO Auto-generated method stub
	 final int R = 6371*1000; // Radious of the earth in meters
	 Double latDistance = toRad(lat2-lat1);
	 Double lonDistance = toRad(lon2-lon1);
	 Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
	 * Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
	 Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	 Double distance = R * c;
	 return distance;
	}



	/**
	 * Imprime el men� por consola
	 */
	private static void printMenu() 
	{
		System.out.println("---------------------Proyecto-2 -- Team 7----------------------");
		System.out.println("0.  Cargar grafo de la malla vial del taller 8");
		System.out.println("1.  Dar punto m�s congestionado en el mapa");
		System.out.println("2.  Dar componentes fuertemente conectadas");
		System.out.println("3.  Graficar punto 2");
		System.out.println("4.  Dar camino de costo minimo entre dos puntos");
		System.out.println("5.  Dar mayor distancia y menor distancia de recorrido de un servicios");
		System.out.println("6.  Calcular si no necesita pagar peajes dado un servicio");
		System.out.println("7.  Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}