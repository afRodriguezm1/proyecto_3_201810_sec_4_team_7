package model.vo;

import model.data_structures.IArco;

public class InfoArco implements IArco {
	
	private double idV1;
	
	private double idV2;

	private int peso;

	private double costo;
	
	private double tiempo;
	
	private double longitud;
	
	private int cantTotal;
	
	public InfoArco(double v1, double v2, Item actual)
	{
		idV1 = v1;
		idV2 = v2;
		this.peso = (int) (actual.getTrip_miles()*1609.34);
		tiempo = 0;
		longitud = 0;
		cantTotal = 1;
		costo = 0;
	}
	
	public void promediarPeso(int nuevoPeso)
	{
		peso = (peso*cantTotal+nuevoPeso)/(cantTotal+1);
	}
	
	public void aumentarCantTotal()
	{
		cantTotal++;
	}
	
	@Override
	public int darPeso() {
		// TODO Auto-generated method stub
		return peso;
	}
	
	public double darV1()
	{
		return idV1;
	}
	
	public double darV2()
	{
		return idV2;
	}

	public void addTiempo(int tiempo) {
		this.tiempo += tiempo;
	}

	public double getLongitud() {
		return longitud;
	}

	public void addLongitud(double longitud) {
		this.longitud += longitud;
	}

	@Override
	public double darTiempo() {
		return tiempo;
	}
	
	public double darCosto()
	{
		return costo;
	}
	
	public void agregarCosto(double pCosto)
	{
		costo += pCosto;	
	}
	
	public void agregarTiempo(double pTiempo)
	{
		tiempo += pTiempo;
	}

}
