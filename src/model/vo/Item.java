package model.vo;

import java.awt.Point;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Item 
{

	/**
	 * Id de la carrera.
	 */
	private String trip_id;

	/**
	 * Id del taxi.
	 */
	private String taxi_id;
	
	/**
	 * Fecha y Hora de inicio de la carrera.
	 */
	private Date trip_start_timestamp;

	/**
	 * Fecha y hora de fin de la carrera.
	 */
	private Date trip_end_timestamp;

	/**
	 * Tiempo en segundos de la carrera.
	 */
	private int trip_seconds;

	/**
	 * Millas de la carrera.
	 */
	private float trip_miles;

	/**
	 * C�digo del area de inicio de la carrera.
	 */
	private int pickup_community_area;

	/**
	 * C�digo del area de fin de la carrera.
	 */
	private int dropoff_community_area;
	
	/**
	 * Costo total de la carrera.
	 */
	private float trip_total;
	
	/**
	 * Compa�ia del taxi.
	 */
	private String company;
	
	/**
	 * Precio que se paga por peaje
	 */
	private double tolls;
	
	/**
	 * Latitud del lugar de inicio de la carrera.
	 */
	private double pickup_centroid_latitude;
	
	/**
	 * Longitud del lugar de inicio de la carrera.
	 */
	private double pickup_centroid_longitude;
	
	/**
	 * Latitud del lugar de fin de la carrera.
	 */
	private double dropoff_centroid_latitude;

	/**
	 * Longitud del lugar de fin de la carrera.
	 */
	private double dropoff_centroid_longitude;
	
	/**
	 * Retorna la Id de la carrera. 
	 * @return trip_id de la carrera.
	 */
	public String getTrip_id() 
	{
		return trip_id;
	}

	/**
	 * Retorna la Id del taxi.
	 * @return taxi_id de la carrera.
	 */
	public String getTaxi_id() 
	{
		return taxi_id;
	}

	/**
	 * Retorna la fecha y hora de inicio de la carrera.
	 * @return trip_start_timestamp de la carrera.
	 */
	public Date getTrip_start_timestamp() 
	{
		return trip_start_timestamp;
	}

	/**
	 * Retorna la fecha y hora de fin de la carrera.
	 * @return trip_end_timestamp de la carrera.
	 */
	public Date getTrip_end_timestamp() 
	{
		return trip_end_timestamp;
	}
	
	/**
	 * Retorna el tiempo en segundos de la carrera.
	 * @return trip_seconds de la carrera.
	 */
	public int getTrip_seconds() 
	{
		return trip_seconds;
	}

	/**
	 * Retorna las millas recorridas en la carrera.
	 * @return trip_miles de la carrera.
	 */
	public float getTrip_miles() 
	{
		return trip_miles;
	}

	/**
	 * Retorna el area donde inici� la carrera.
	 * @return pickup_community_area de la carrera.
	 */
	public int getPickup_community_area() 
	{
		return pickup_community_area;
	}
	
	/**
	 * Retorna el area donde finaliz� la carrera.
	 * @return dropoff_community_area de la carrera.
	 */
	public int getDropoff_community_area() 
	{
		return dropoff_community_area;
	}

	/**
	 * Retorna el precio total de la carrera.
	 * @return trip_total de la carrera.
	 */
	public float getTrip_total() 
	{
		return trip_total;
	}

	/**
	 * Retorna la compa�ia del taxi de la carrera.
	 * @return company del taxi de la carrera.
	 */
	public String getCompany() 
	{
		return company;
	}

	/**
	 * Retorna la latitud de inicio de la carrera.
	 * @return pickup_centroid_latitude de la carrera.
	 */
	public double getPickup_centroid_latitude() 
	{
		return pickup_centroid_latitude;
	}

	/**
	 * Retorna la longitud de inicio de la carrera.
	 * @return pickup_centroid_longitude de la carrera.
	 */
	public double getPickup_centroid_longitude() 
	{
		return pickup_centroid_longitude;
	}
	
	/**
	 * Retorna la latitud de fin de la carrera.
	 * @return dropoff_centroid_latitude de la carrera.
	 */
	public double getDropoff_centroid_latitude() 
	{
		return dropoff_centroid_latitude;
	}

	/**
	 * Return longitud de fin de la carrera.
	 * @return dropoff_centroid_lungitude de la carrera.
	 */
	public double getDropoff_centroid_longitude() 
	{
		return dropoff_centroid_longitude;
	}
	
	public double getTolls()
	{
		return tolls;
	}

	public void setTrip_end_timestamp(Date trip_end_timestamp) 
	{
		
		this.trip_end_timestamp = (this.trip_end_timestamp.before(trip_end_timestamp)? trip_end_timestamp : this.trip_end_timestamp);
	}

	public void setTrip_seconds(int trip_seconds) 
	{
		this.trip_seconds += trip_seconds;
	}

	public void setTrip_miles(float trip_miles) 
	{
		this.trip_miles += trip_miles;
	}

	public void setTrip_total(float trip_total) 
	{
		this.trip_total += trip_total;
	}
	
	public void setTrip_start_timestamp(Date pFecha)
	{
		this.trip_start_timestamp = pFecha;
	}
	
	public int hashCode()
	{
		return trip_id.hashCode();
	}
	
	
}