package model.vo;

import model.data_structures.IVertice;
import model.data_structures.Lista;

public class VerticeItem implements IVertice<Double>{

	private double longRef;
	
	private double latRef;
	
	private boolean marcado;
	
	private Lista<Item> items;
	
	public Lista<Item> darItems()
	{
		return items;
	}
	
	public void agregarItem(Item v){
		items.agregar(v);
	}
	
	public VerticeItem(double latRef, double longRef, Item v)
	{
		items = new Lista<Item>();
		items.agregar(v);
		this.latRef = latRef;
		this.longRef = longRef;
	}
	
	public double darLatref()
	{
		return latRef;
	}
	
	public double longRef()
	{
		return longRef;
	}
	
	private double id()
	{
		double dist = getDistance(41.8,-87.6,latRef, longRef);
		return dist;
	}

	private double toRad(double numero)
	{
		return Math.toRadians(numero);
	}
	
	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
	 // TODO Auto-generated method stub
	 final int R = 6371*1000; // Radious of the earth in meters
	 Double latDistance = toRad(lat2-lat1);
	 Double lonDistance = toRad(lon2-lon1);
	 Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
	 * Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
	 Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	 Double distance = R * c;
	 return distance;
	}
	
	public Double darId2(double lat1, double lon1) 
	{
		return getDistance(lat1,lon1,latRef, longRef);
	}
	@Override
	public Double darId() {
		
		return id();
	}

	@Override
	public void marcar() {
		// TODO Auto-generated method stub
		marcado = true;
	}


}
