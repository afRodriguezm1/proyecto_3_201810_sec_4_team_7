package model.logic;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.sun.java_cup.internal.runtime.Symbol;

import model.data_structures.*;
import model.vo.InfoArco;
import model.vo.Item;
import model.vo.VerticeItem;

public class Manager<Key, Value, K, V>
{
	/**
	 * Grafo dirigido de los servicios.
	 */	
	private GrafoDirigido<Double, VerticeItem, InfoArco> grafo;

	/**
	 * Ta�o de informaci�n de los datos cargados
	 */
	private String tamanio;
	
	/**
	 * Distancia alrededor de un vertice.
	 */
	private int distancia;
	
	/**
	 * Estructura kosaraju para componentes fuertemente conexos
	 */
	private Kosaraju<Integer> kosaraju;

	/**
	 * Comberte en radianes el numero que llega por parametro
	 * @param numero double con el n�mero que se desea convertir a radianes
	 * @return radianes del numero que llega por parametro
	 */
	private double toRad(double numero)
	{
		return Math.toRadians(numero);
	}

	/**
	 * Da la distancia al rededor de un vertice.
	 * @return distancia.
	 */
	public double darDistancia()
	{
		return distancia;
	}

	/**
	 * Retorna el vertice con mayor n�mero de servicios de salida y llegada
	 * @return VerticeItem con mayor servicios de salida y llegada
	 */
	public VerticeItem req1()
	{
		int mayorCantidad = 0;
		VerticeItem mayor = null;
		Lista<VerticeItem> lista = grafo.darVertices();
		for(int i = 0; i < lista.darLongitud(); i++)
		{
			VerticeItem actual = lista.darElemento(i);
			if(actual!=null)
			{
				if(actual.darItems().darLongitud() > mayorCantidad)
				{
					mayor = actual;
					mayorCantidad = actual.darItems().darLongitud();
				}
			}
		}
		graficarReq1(mayor);
		return mayor;
	}

	/**
	 * Retorna y grafica el componente fuertemente conectado m�s grande del grafo
	 */
	public void req2()
	{
		kosaraju = new Kosaraju(grafo);
		Lista grupos = kosaraju.grupos();
		int max = 0;
		int numGrupo = 0;
		Lista maxGrupo = null;
		for(int i = 0; i < grupos.darLongitud(); i++)
		{
			if(((Lista)grupos.darElemento(i)).darLongitud() > max)
			{
				maxGrupo = (Lista) grupos.darElemento(i);
				max = maxGrupo.darLongitud();
				numGrupo = i;
			}
	
		}
		System.out.println("Hay " + kosaraju.count() + " componentes fuertemente conectados y el componente m�s grande es el " + numGrupo + " con " + maxGrupo.darLongitud() + " vertices");
		File file = null;
		try 
		{
			file = new File("./data/maps/requerimiento2.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>Requerimiento 2</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      var citymap = {");
			for(int i = 0 ; i < maxGrupo.darLongitud(); i++)
			{
				VerticeItem vertice = (VerticeItem) ((Vertice)grafo.darObjetoVertice(maxGrupo.darElemento(i))).darInfoVertice();
				print.write("        "+ i + ": {\r\n" + 
						"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
						"          population: 200,\r\n" + 
						"          color: '#FF0000'\r\n" +
						"        }" + ((i == (maxGrupo.darLongitud()-1))?" ": ",") + "\r\n" + 
						""); 
			}
			print.write("      };\r\n" + 
					"\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });\r\n" + 
					"        for (var city in citymap) {\r\n" + 
					"          var cityCircle = new google.maps.Circle({\r\n" + 
					"            strokeColor: citymap[city].color," + 
					"            strokeOpacity: 0.8,\r\n" + 
					"            strokeWeight: 2,\r\n" + 
					"            fillColor: '#FF0000',\r\n" + 
					"            fillOpacity: 0.35,\r\n" + 
					"            map: map,\r\n" + 
					"            center: citymap[city].center,\r\n" + 
					"            radius: citymap[city].population\r\n" + 
					"          });\r\n" + 
					"        }\r\n");
			print.write("        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");
			for(int i = 0; i < maxGrupo.darLongitud(); i++)
			{
				Vertice<Double, VerticeItem, InfoArco> vertice = grafo.darObjetoVertice(maxGrupo.darElemento(i));
				ArrayList adj = vertice.darSucesores();
				VerticeItem verticeA = (VerticeItem) vertice.darInfoVertice();
				for(int j = 0; j < adj.size(); j++)
				{
					Arco<K, IVertice<K>, IArco> arco = (Arco<K, IVertice<K>, IArco>) adj.get(j);
					VerticeItem verticeB = (VerticeItem) ((Vertice)arco.darVerticeDestino()).darInfoVertice();
					boolean esta = false;
					for(int a = 0; a < maxGrupo.darLongitud(); a++)
					{
						if(maxGrupo.darElemento(a).equals(verticeB.darId()))
							esta = true;
					}
					if(esta == false)
						continue;
					Lista puntos = puntosNuevos(verticeA.darLatref(), verticeA.longRef(), verticeB.darLatref(), verticeB.longRef());
					print.write("var line = new google.maps.Polyline({\r\n" + 
							"          path: [{lat: "+ puntos.darElemento(0)+", lng: " + puntos.darElemento(1) + "}, {lat: " + puntos.darElemento(2) +", lng: " + puntos.darElemento(3) +"}],\r\n" + 
							"          icons: [{\r\n" + 
							"          icon: lineSymbol,\r\n" + 
							"          offset: '100%'\r\n" + 
							"          }],\r\n" +
							"          map: map\r\n" + 
							"        });");
				}
			}
			print.write("      }\r\n" + 
			"    </script>\r\n" + 
			"    <script async defer\r\n" + 
			"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
			"    </script>\r\n" + 
			"  </body>\r\n" + 
			"</html>\r\n" + 
			"");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				// Runtime.getRuntime().exec("open -a safari " + url);
				// Runtime.getRuntime().exec("open " + url + "/index.html");
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * Retorna y grafica todos los componentes fuertemente conectados y los representa de colores.
	 */
	public void req3()
	{
		if(kosaraju == null)
			kosaraju = new Kosaraju(grafo);
		int mayor = 0;
		Lista vertices = grafo.darVertices();
		for(int i = 0; i< vertices.darLongitud(); i++)
		{
			VerticeItem vertex = (VerticeItem) vertices.darElemento(i);
			if(mayor < vertex.darItems().darLongitud())
				mayor = vertex.darItems().darLongitud();
		}
		File file = null;
		Lista grupos = kosaraju.grupos();
		try 
		{
			file = new File("./data/maps/requerimiento3.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>Requerimiento 3</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });" + 
					"        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");
			for(int i = 0 ; i < grupos.darLongitud(); i++)
			{
				Color color = new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255));
				String stringColor = Integer.toHexString(color.getRGB());
				stringColor = stringColor.substring(2, 8);
				Lista grupo = (Lista) grupos.darElemento(i);
				for(int j = 0; j < grupo.darLongitud(); j++)
				{
					VerticeItem verticeItem = (VerticeItem) ((Vertice)grafo.darObjetoVertice(grupo.darElemento(j))).darInfoVertice();
					int radio = (verticeItem.darItems().darLongitud()*1000)/mayor;
					print.write("var cityCircle = new google.maps.Circle({\r\n" + 
							"            strokeColor: '#"+ stringColor + "',\r\n" + 
							"            strokeOpacity: 0.8,\r\n" + 
							"            strokeWeight: 2,\r\n" + 
							"            fillColor: '#"+ stringColor + "',\r\n" + 
							"            fillOpacity: 0.35,\r\n" + 
							"            map: map,\r\n" + 
							"            center: {lat: " + verticeItem.darLatref() +", lng: " + verticeItem.longRef() + "},\r\n" + 
							"            radius: " + ((radio < 1)? "1": radio) +",\r\n" + 
							"          });");
					
					Vertice<Double, VerticeItem, InfoArco> vertice = grafo.darObjetoVertice(grupo.darElemento(j));
					ArrayList adj = vertice.darSucesores();
					VerticeItem verticeA = (VerticeItem) vertice.darInfoVertice();
					for(int x = 0; x < adj.size(); x++)
					{
						Arco<K, IVertice<K>, IArco> arco = (Arco<K, IVertice<K>, IArco>) adj.get(x);
						VerticeItem verticeB = (VerticeItem) ((Vertice)arco.darVerticeDestino()).darInfoVertice();
						boolean esta = false;
						int radioB = (verticeB.darItems().darLongitud()*1000)/mayor;
						Lista puntos = puntosNuevos(verticeA.darLatref(), verticeA.longRef(), verticeB.darLatref(), verticeB.longRef(),((radio < 1)? 1: radio), ((radioB < 1)? 1: radioB));
						print.write("var line = new google.maps.Polyline({\r\n" + 
								"          path: [{lat: "+ puntos.darElemento(0)+", lng: " + puntos.darElemento(1) + "}, {lat: " + puntos.darElemento(2) +", lng: " + puntos.darElemento(3) +"}],\r\n" + 
								"          icons: [{\r\n" + 
								"          icon: lineSymbol,\r\n" + 
								"          offset: '100%'\r\n" + 
								"          }],\r\n" +
								"          strokeColor: '#" + stringColor + "',"+
								"          map: map\r\n" + 
								"        });");
					}
				}
			}
			print.write("}\r\n" + 
					"    </script>\r\n" + 
					"    <script async defer\r\n" + 
					"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
					"    </script>\r\n" + 
					"  </body>\r\n" + 
					"</html>");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * Busca un camino entre dos puntos aleatoreos escogidos del csv
	 * @return Camino con los arcos y vertices del trayecto entre los dos vertices aleatoreos
	 * @throws VerticeNoExisteException
	 * @throws IOException
	 */
	public Camino<Double,VerticeItem,InfoArco> req4() throws VerticeNoExisteException, IOException
	{
		FileReader csv = new FileReader("./data/Chicago Streets.csv");
		BufferedReader reader = new BufferedReader(csv);
		int num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		int i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		String linea = reader.readLine();
		String[] arreglo = linea.split(";");
		String[] latlong = arreglo[6].split(" ");
		double lat = Double.parseDouble(latlong[1]);
		double longitud = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();
		
		csv = new FileReader("./data/Chicago Streets.csv");
		reader = new BufferedReader(csv);
		num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		linea = reader.readLine();
		arreglo = linea.split(";");
		latlong = arreglo[6].split(" ");
		double lat2 = Double.parseDouble(latlong[1]);
		double longitud2 = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();
		
		Lista<VerticeItem> vertices = grafo.darVertices();
		VerticeItem masCercano = null;
		VerticeItem masCercanoFin = null;
		double dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat, longitud)/1000000;
	
			if(distref <= dist)
			{
				dist = distref;
				masCercano = vertices.darElemento(j);
			}
		}
		dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref2 = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat2, longitud2)/1000000;
	
			if(distref2 <= dist)
			{
				dist = distref2;
				masCercanoFin = vertices.darElemento(j);
			}
		}
		Camino<Double,VerticeItem,InfoArco> r = grafo.darCaminoMasBarato(masCercano.darId(), masCercanoFin.darId());
		if(r!=null)
		{
			graficarReq4(r, masCercano, masCercanoFin);
			Lista<InfoArco> arcos = r.darSecuenciaArcos();
			for(int j = 0; j<arcos.darLongitud(); j++)
			{
				arcos.darElemento(j).agregarTiempo(r.darVertices().darElemento(j).darItems().darElemento(0).getTrip_seconds());
				arcos.darElemento(j).agregarCosto(r.darVertices().darElemento(j).darItems().darElemento(0).getTrip_total());
			}
			r.cambiarArcos(arcos);
		}
		return r;
	}

	/**
	 * Busca un camino de ida y de regreso entre dos puntos aleatoreos del csv
	 * @return Camino[] con la serie de arcos y vertices de ida y de regreso
	 * @throws IOException
	 * @throws VerticeNoExisteException
	 */
	public Camino<Double,VerticeItem,InfoArco>[] req5() throws IOException, VerticeNoExisteException
	{

		FileReader csv = new FileReader("./data/Chicago Streets.csv");
		BufferedReader reader = new BufferedReader(csv);
		int num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		int i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		String linea = reader.readLine();
		String[] arreglo = linea.split(";");
		String[] latlong = arreglo[6].split(" ");
		double lat = Double.parseDouble(latlong[1]);
		double longitud = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();
		
		csv = new FileReader("./data/Chicago Streets.csv");
		reader = new BufferedReader(csv);
		num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		linea = reader.readLine();
		arreglo = linea.split(";");
		latlong = arreglo[6].split(" ");
		double lat2 = Double.parseDouble(latlong[1]);
		double longitud2 = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();
		
		Lista<VerticeItem> vertices = grafo.darVertices();
		VerticeItem masCercano = null;
		VerticeItem masCercanoFin = null;
		double dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat, longitud)/1000000;

			if(distref <= dist)
			{
				dist = distref;
				masCercano = vertices.darElemento(j);
			}
		}
		dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref2 = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat2, longitud2)/1000000;

			if(distref2 <= dist)
			{
				dist = distref2;
				masCercanoFin = vertices.darElemento(j);
			}
		}
		Camino<Double,VerticeItem,InfoArco> r1 = grafo.darCaminoMasBarato(masCercano.darId(), masCercanoFin.darId());
		Camino<Double,VerticeItem,InfoArco> r2 = grafo.darCaminoMasBarato(masCercanoFin.darId(), masCercano.darId());
		if(r1!=null || r2 != null)
			graficarReq5(r1, r2, masCercano, masCercanoFin);
		if(r1!=null)
		{
			Lista<InfoArco> arcos = r1.darSecuenciaArcos();
			for(int j = 0; j<arcos.darLongitud(); j++)
			{
				arcos.darElemento(j).agregarTiempo(r1.darVertices().darElemento(j).darItems().darElemento(0).getTrip_seconds());
				arcos.darElemento(j).agregarCosto(r1.darVertices().darElemento(j).darItems().darElemento(0).getTrip_total());
			}
			r1.cambiarArcos(arcos);
		}
		if(r2!=null)
		{
			Lista<InfoArco> arcos = r2.darSecuenciaArcos();
			for(int j = 0; j<arcos.darLongitud(); j++)
			{
				arcos.darElemento(j).agregarTiempo(r2.darVertices().darElemento(j).darItems().darElemento(0).getTrip_seconds());
				arcos.darElemento(j).agregarCosto(r2.darVertices().darElemento(j).darItems().darElemento(0).getTrip_total());
			}
			r2.cambiarArcos(arcos);
		}
		Camino<Double,VerticeItem,InfoArco>[] r = new Camino[2];
		r[0] = r1;
		r[1] = r2;
		return r;
	}
	
	/**
	 * Busca el camino m�s corto sin peajes entre dos puntos aleatoreos sacados del csv
	 * @return Lista[] con los caminos ordenados crecientemente por tiempo y decrecientemente por distancia
	 * @throws IOException
	 * @throws VerticeNoExisteException
	 * @throws ArcoNoExisteException
	 */
	public Lista[] req6() throws IOException, VerticeNoExisteException, ArcoNoExisteException
	{
		Lista[] arregloFinal = new Lista[2];
		FileReader csv = new FileReader("./data/Chicago Streets.csv");
		BufferedReader reader = new BufferedReader(csv);
		int num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		int i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		String linea = reader.readLine();
		String[] arreglo = linea.split(";");
		String[] latlong = arreglo[6].split(" ");
		double lat = Double.parseDouble(latlong[1]);
		double longitud = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();

		csv = new FileReader("./data/Chicago Streets.csv");
		reader = new BufferedReader(csv);
		num = (int) (Math.random()*36339);
		if(num == 0)
			num = 1;
		i = 0;
		while(i < num)
		{
			reader.readLine();		
			i++;
		}
		linea = reader.readLine();
		arreglo = linea.split(";");
		latlong = arreglo[6].split(" ");
		double lat2 = Double.parseDouble(latlong[1]);
		double longitud2 = Double.parseDouble(latlong[0]);
		csv.close();
		reader.close();

		Lista<VerticeItem> vertices = grafo.darVertices();
		VerticeItem masCercano = null;
		VerticeItem masCercanoFin = null;
		double dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat, longitud)/1000000;

			if(distref <= dist)
			{
				dist = distref;
				masCercano = vertices.darElemento(j);
			}
		}
		dist = Double.MAX_VALUE;
		for(int j = 0; j < grafo.V(); j++)
		{
			double distref2 = Math.random()*getDistance(vertices.darElemento(j).darLatref(), vertices.darElemento(j).longRef(), lat2, longitud2)/1000000;

			if(distref2 <= dist)
			{
				dist = distref2;
				masCercanoFin = vertices.darElemento(j);
			}
		}
		DFSCaminos<K> dfs = new DFSCaminos(grafo, masCercano.darId(), masCercanoFin.darId());
		Lista resultado = dfs.darCaminos();
		Lista aux = sort(resultado, 0);
		Lista aux2 = sort(resultado, 1);
		arregloFinal[0] = aux;
		arregloFinal[1] = aux2;
		if(resultado.darLongitud()>0)
			graficarReq6( (Camino<Double, VerticeItem, InfoArco>)aux.darElemento(0), (Camino<Double, VerticeItem, InfoArco>)aux2.darElemento((int)aux.darLongitud()/2));
		return arregloFinal;
	}

	/**
	 * Grafica el requerimiento 6
	 * @param cam1 Camino con el menor tiempo
	 * @param cam2 Camino con la mayor distancia
	 */
	private void graficarReq6(Camino<Double, VerticeItem, InfoArco> cam1, Camino<Double, VerticeItem, InfoArco> cam2)
	{
		File file = null;
		try 
		{
			file = new File("./data/maps/requerimiento6.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>Requerimiento 6</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });" + 
					"        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");
			Lista vertices = cam1.darVertices();
			for(int i = 0 ; i < vertices.darLongitud(); i++)
			{
				VerticeItem verticeItem = (VerticeItem) vertices.darElemento(i);
				print.write("var cityCircle = new google.maps.Circle({\r\n" + 
						"            strokeColor: '#FF0000',\r\n" + 
						"            strokeOpacity: 0.8,\r\n" + 
						"            strokeWeight: 2,\r\n" + 
						"            fillColor: '#FF0000',\r\n" + 
						"            fillOpacity: 0.35,\r\n" + 
						"            map: map,\r\n" + 
						"            center: {lat: " + verticeItem.darLatref() +", lng: " + verticeItem.longRef() + "},\r\n" + 
						"            radius: 100,\r\n" + 
						"          });");
			}
			vertices = cam2.darVertices();
			for(int i = 0 ; i < vertices.darLongitud(); i++)
			{
				VerticeItem verticeItem = (VerticeItem) vertices.darElemento(i);
				print.write("var cityCircle = new google.maps.Circle({\r\n" + 
						"            strokeColor: '#0000FF',\r\n" + 
						"            strokeOpacity: 0.8,\r\n" + 
						"            strokeWeight: 2,\r\n" + 
						"            fillColor: '#0000FF',\r\n" + 
						"            fillOpacity: 0.35,\r\n" + 
						"            map: map,\r\n" + 
						"            center: {lat: " + verticeItem.darLatref() +", lng: " + verticeItem.longRef() + "},\r\n" + 
						"            radius: 100,\r\n" + 
						"          });");
			}
			Lista arcos = cam1.darSecuenciaArcos();
			for(int i = 0 ; i < vertices.darLongitud(); i++)
			{
				InfoArco arco =  (InfoArco) arcos.darElemento(i);
				VerticeItem verticeA = (VerticeItem) ((Vertice)grafo.darObjetoVertice(arco.darV1())).darInfoVertice();
				VerticeItem verticeB = (VerticeItem) ((Vertice)grafo.darObjetoVertice(arco.darV2())).darInfoVertice();
				Lista puntos = puntosNuevos(verticeA.darLatref(), verticeA.longRef(), verticeB.darLatref(), verticeB.longRef(), 100, 100);
				print.write("var line = new google.maps.Polyline({\r\n" + 
						"          path: [{lat: "+ puntos.darElemento(0)+", lng: " + puntos.darElemento(1) + "}, {lat: " + puntos.darElemento(2) +", lng: " + puntos.darElemento(3) +"}],\r\n" + 
						"          icons: [{\r\n" + 
						"          icon: lineSymbol,\r\n" + 
						"          offset: '100%'\r\n" + 
						"          }],\r\n" +
						"          strokeColor: '#FF0000',"+
						"          map: map\r\n" + 
						"        });");
			}
			arcos = cam2.darSecuenciaArcos();
			for(int i = 0 ; i < vertices.darLongitud(); i++)
			{
				InfoArco arco =  (InfoArco) arcos.darElemento(i);
				VerticeItem verticeA = (VerticeItem) ((Vertice)grafo.darObjetoVertice(arco.darV1())).darInfoVertice();
				VerticeItem verticeB = (VerticeItem) ((Vertice)grafo.darObjetoVertice(arco.darV2())).darInfoVertice();
				Lista puntos = puntosNuevos(verticeA.darLatref(), verticeA.longRef(), verticeB.darLatref(), verticeB.longRef(), 100, 100);
				print.write("var line = new google.maps.Polyline({\r\n" + 
						"          path: [{lat: "+ puntos.darElemento(0)+", lng: " + puntos.darElemento(1) + "}, {lat: " + puntos.darElemento(2) +", lng: " + puntos.darElemento(3) +"}],\r\n" + 
						"          icons: [{\r\n" + 
						"          icon: lineSymbol,\r\n" + 
						"          offset: '100%'\r\n" + 
						"          }],\r\n" +
						"          strokeColor: '#FFFFF',"+
						"          map: map\r\n" + 
						"        });");
			}
			print.write("}\r\n" + 
					"    </script>\r\n" + 
					"    <script async defer\r\n" + 
					"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
					"    </script>\r\n" + 
					"  </body>\r\n" + 
					"</html>");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}
	
	/**
	 * Grafica el requerimiento 5
	 * @param r1 Camino con el camino de ida
	 * @param r2 Camino con el camino de regreso
	 * @param masCercano Vertice desde donde comienza
	 * @param masCercanoFin Vertice desde donde termina
	 */
	private void graficarReq5(Camino<Double,VerticeItem,InfoArco> r1, Camino<Double,VerticeItem,InfoArco> r2, VerticeItem masCercano, VerticeItem masCercanoFin)
	{
		File file = null;
		try 
		{
			file = new File("./data/maps/requerimiento5.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>Requerimiento 5</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      var citymap = {");
			if(r1 == null && r2 == null)
			{
			print.write("        " + 900 + ": {\r\n" + 
					"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
					"          population: "+ distancia + ",\r\n" + 
					"          color: '#FF0000'\r\n" +
					"        }" +"," + "\r\n" +
					""); 
			print.write("        " + 901 + ": {\r\n" + 
					"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
					"          population: "+ distancia + ",\r\n" + 
					"          color: '#FF0000'\r\n" +
					"        }" + " " +  "\r\n" +
					""); 
			}
			else if (r1 != null && r2 == null)
			{
				print.write("        " + 900 + ": {\r\n" + 
						"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" +"," + "\r\n" +
						""); 
				Lista<VerticeItem> lista = r1.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);;
					print.write("        "+ i + ": {\r\n" + 
							"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
							"          population: "+ distancia + ",\r\n" + 
							"          color: '#FF0000'\r\n" +
							"        }" + "," + "\r\n" + 
							""); 
				}
				print.write("        " + 901 + ": {\r\n" + 
						"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" + " " +  "\r\n" +
						""); 
			}
			else if (r1 == null && r2 != null)
			{
				print.write("        " + 900 + ": {\r\n" + 
						"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" +"," + "\r\n" +
						""); 
				Lista<VerticeItem> lista = r2.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);;
					print.write("        "+ i + ": {\r\n" + 
							"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
							"          population: "+ distancia + ",\r\n" + 
							"          color: '#00FF00'\r\n" +
							"        }" + "," + "\r\n" + 
							""); 
				}
				print.write("        " + 901 + ": {\r\n" + 
						"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" + " " +  "\r\n" +
						""); 
			}
			else
			{
				print.write("        " + 900 + ": {\r\n" + 
						"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" +"," + "\r\n" +
						""); 
				Lista<VerticeItem> lista = r1.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);;
					print.write("        "+ i + ": {\r\n" + 
							"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
							"          population: "+ distancia + ",\r\n" + 
							"          color: '#FF0000'\r\n" +
							"        }" + "," + "\r\n" + 
							""); 
				}
				Lista<VerticeItem> lista2 = r2.darVertices();
				for(int i = 0; i < lista2.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista2.darElemento(i);;
					print.write("        "+ i + ": {\r\n" + 
							"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
							"          population: "+ distancia + ",\r\n" + 
							"          color: '#00FF00'\r\n" +
							"        }" + "," + "\r\n" + 
							""); 
				}
				print.write("        " + 901 + ": {\r\n" + 
						"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" + " " +  "\r\n" +
						""); 
			}
			print.write("      };\r\n" + 
					"\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });\r\n" + 
					"        for (var city in citymap) {\r\n" + 
					"          var cityCircle = new google.maps.Circle({\r\n" + 
					"            strokeColor: citymap[city].color," + 
					"            strokeOpacity: 0.8,\r\n" + 
					"            strokeWeight: 2,\r\n" + 
					"            fillColor: '#FF0000',\r\n" + 
					"            fillOpacity: 0.35,\r\n" + 
					"            map: map,\r\n" + 
					"            center: citymap[city].center,\r\n" + 
					"            radius: Math.sqrt(citymap[city].population) * 100\r\n" + 
					"          });\r\n" + 
					"        }\r\n");
			print.write("        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");
			if(r1!=null)
			{
				Lista<VerticeItem> lista = r1.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);
					VerticeItem vertice2 = lista.darElemento(i+1);	
					print.write("var line = new google.maps.Polyline({\r\n" + 
							"          path: [{lat: "+ vertice.darLatref()+", lng: " + vertice.longRef() + "}, {lat: " + vertice2.darLatref() +", lng: " + vertice2.longRef() +"}],\r\n" + 
							"			strokeColor: '#FF0000',\r\n"+
							"			strokeOpacity: 1.0,\r\n"+
							"			strokeWeight: 2,\r\n"+
							"          icons: [{\r\n" + 
							"          icon: lineSymbol,\r\n" + 
							"          offset: '100%'\r\n" + 
							"          }],\r\n" +
							"          map: map\r\n" + 
							"        });");
				}
				print.write("var line = new google.maps.Polyline({\r\n" + 
						"          path: [{lat: "+ masCercano.darLatref()+", lng: " + masCercano.longRef() + "}, {lat: " + r1.darVertices().darElemento(0).darLatref() +", lng: " + r1.darVertices().darElemento(0).longRef() +"}],\r\n" + 
						"			strokeColor: '#FF0000',\r\n"+
						"			strokeOpacity: 1.0,\r\n"+
						"			strokeWeight: 2,\r\n"+
						"          icons: [{\r\n" + 
						"          icon: lineSymbol,\r\n" + 
						"          offset: '100%'\r\n" + 
						"          }],\r\n" +
						"          map: map\r\n" + 
						"        });");
			}
			if(r2 != null)
			{
				Lista<VerticeItem> lista = r2.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);
					VerticeItem vertice2 = lista.darElemento(i+1);	
					print.write("var line = new google.maps.Polyline({\r\n" + 
							"          path: [{lat: "+ vertice.darLatref()+", lng: " + vertice.longRef() + "}, {lat: " + vertice2.darLatref() +", lng: " + vertice2.longRef() +"}],\r\n" + 
							"			strokeColor: '#00FF00',\r\n"+
							"			strokeOpacity: 1.0,\r\n"+
							"			strokeWeight: 2,\r\n"+
							"          icons: [{\r\n" + 
							"          icon: lineSymbol,\r\n" + 
							"          offset: '100%'\r\n" + 
							"          }],\r\n" +
							"          map: map\r\n" + 
							"        });");
				}
				print.write("var line = new google.maps.Polyline({\r\n" + 
						"          path: [{lat: "+ masCercanoFin.darLatref() +", lng: " + masCercanoFin.longRef() + "}, {lat: " + r2.darVertices().darElemento(0).darLatref() +", lng: " + r2.darVertices().darElemento(0).longRef() +"}],\r\n" + 
						"			strokeColor: '#00FF00',\r\n"+
						"			strokeOpacity: 1.0,\r\n"+
						"			strokeWeight: 2,\r\n"+
						"          icons: [{\r\n" + 
						"          icon: lineSymbol,\r\n" + 
						"          offset: '100%'\r\n" + 
						"          }],\r\n" +
						"          map: map\r\n" + 
						"        });");
			}
			print.write("      }\r\n" + 
					"    </script>\r\n" + 
					"    <script async defer\r\n" + 
					"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
					"    </script>\r\n" + 
					"  </body>\r\n" + 
					"</html>\r\n" + 
					"");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				// Runtime.getRuntime().exec("open -a safari " + url);
				// Runtime.getRuntime().exec("open " + url + "/index.html");
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * Grafica el requerimiento 4
	 * @param r Camino entre los dos puntos
	 * @param masCercano Vertice desde donde comienza
	 * @param masCercanoFin Vertice desde donde termina
	 */
	private void graficarReq4(Camino<Double,VerticeItem,InfoArco> r, VerticeItem masCercano, VerticeItem masCercanoFin)
	{
		File file = null;
		try 
		{
			file = new File("./data/maps/requerimiento4.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>requerimiento 4</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      var citymap = {");
			if(r == null)
			{
			print.write("        " + 900 + ": {\r\n" + 
					"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
					"          population: "+ distancia + ",\r\n" + 
					"          color: '#FF0000'\r\n" +
					"        }" +"," + "\r\n" +
					""); 
			print.write("        " + 901 + ": {\r\n" + 
					"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
					"          population: "+ distancia + ",\r\n" + 
					"          color: '#FF0000'\r\n" +
					"        }" + " " +  "\r\n" +
					""); 
			}
			else
			{
				print.write("        " + 900 + ": {\r\n" + 
						"          center: {lat: " + masCercano.darLatref() + ", lng: "+ masCercano.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" +"," + "\r\n" +
						""); 
				Lista<VerticeItem> lista = r.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);;
					print.write("        "+ i + ": {\r\n" + 
							"          center: {lat: " + vertice.darLatref() + ", lng: "+ vertice.longRef()+ "},\r\n" + 
							"          population: "+ distancia + ",\r\n" + 
							"          color: '#FF0000'\r\n" +
							"        }" + "," + "\r\n" + 
							""); 
				}
				print.write("        " + 901 + ": {\r\n" + 
						"          center: {lat: " + masCercanoFin.darLatref() + ", lng: "+ masCercanoFin.longRef()+ "},\r\n" + 
						"          population: "+ distancia + ",\r\n" + 
						"          color: '#0000FF'\r\n" +
						"        }" + " " +  "\r\n" +
						""); 
			}
			print.write("      };\r\n" + 
					"\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });\r\n" + 
					"        for (var city in citymap) {\r\n" + 
					"          var cityCircle = new google.maps.Circle({\r\n" + 
					"            strokeColor: citymap[city].color," + 
					"            strokeOpacity: 0.8,\r\n" + 
					"            strokeWeight: 2,\r\n" + 
					"            fillColor: '#FF0000',\r\n" + 
					"            fillOpacity: 0.35,\r\n" + 
					"            map: map,\r\n" + 
					"            center: citymap[city].center,\r\n" + 
					"            radius: Math.sqrt(citymap[city].population) * 100\r\n" + 
					"          });\r\n" + 
					"        }\r\n");
			print.write("        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");
			if(r!=null)
			{
				Lista<VerticeItem> lista = r.darVertices();
				for(int i = 0; i < lista.darLongitud()-1; i++)
				{
					VerticeItem vertice = lista.darElemento(i);
					VerticeItem vertice2 = lista.darElemento(i+1);	
					print.write("var line = new google.maps.Polyline({\r\n" + 
							"          path: [{lat: "+ vertice.darLatref()+", lng: " + vertice.longRef() + "}, {lat: " + vertice2.darLatref() +", lng: " + vertice2.longRef() +"}],\r\n" + 
							"          icons: [{\r\n" + 
							"          icon: lineSymbol,\r\n" + 
							"          offset: '100%'\r\n" + 
							"          }],\r\n" +
							"          map: map\r\n" + 
							"        });");
				}
				print.write("var line = new google.maps.Polyline({\r\n" + 
						"          path: [{lat: "+ masCercano.darLatref()+", lng: " + masCercano.longRef() + "}, {lat: " + r.darVertices().darElemento(0).darLatref() +", lng: " + r.darVertices().darElemento(0).longRef() +"}],\r\n" + 
						"          icons: [{\r\n" + 
						"          icon: lineSymbol,\r\n" + 
						"          offset: '100%'\r\n" + 
						"          }],\r\n" +
						"          map: map\r\n" + 
						"        });");
			}
			print.write("      }\r\n" + 
					"    </script>\r\n" + 
					"    <script async defer\r\n" + 
					"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
					"    </script>\r\n" + 
					"  </body>\r\n" + 
					"</html>\r\n" + 
					"");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				// Runtime.getRuntime().exec("open -a safari " + url);
				// Runtime.getRuntime().exec("open " + url + "/index.html");
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	/**
	 * Grafica el requerimiento 1
	 * @param mayor Vertice con mayor n�mero de servicos de salida y de llegada
	 */
	private void graficarReq1(VerticeItem mayor)
	{
		File file = null;
		try 
		{
			file = new File("./data/maps/requerimiento1.html");
			FileWriter print = new FileWriter(file);
			print.write("<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"  <head>\r\n" + 
					"    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\r\n" + 
					"    <meta charset=\"utf-8\">\r\n" + 
					"    <title>Map</title>\r\n" + 
					"    <style>\r\n" + 
					"      #map {\r\n" + 
					"        height: 100%;\r\n" + 
					"      }\r\n" + 
					"      html, body {\r\n" + 
					"        height: 100%;\r\n" + 
					"        margin: 0;\r\n" + 
					"        padding: 0;\r\n" + 
					"      }\r\n" + 
					"    </style>\r\n" + 
					"  </head>\r\n" + 
					"  <body>\r\n" + 
					"    <div id=\"map\"></div>\r\n" + 
					"    <script>\r\n" + 
					"      var citymap = {");
			print.write("        " + 0 + ": {\r\n" + 
					"          center: {lat: " + mayor.darLatref() + ", lng: "+ mayor.longRef()+ "},\r\n" + 
					"          population: "+ distancia/2 + ",\r\n" + 
					"          color: '#FF0000'\r\n" +
					"        }" + " " +  "\r\n" +
					""); 
			print.write("      };\r\n" + 
					"\r\n" + 
					"      function initMap() {\r\n" + 
					"        var map = new google.maps.Map(document.getElementById('map'), {\r\n" + 
					"          zoom: 11,\r\n" + 
					"          center: {lat: 41.8332884, lng: -87.7562045},\r\n" + 
					"          mapTypeId: 'terrain'\r\n" + 
					"        });\r\n" + 
					"        for (var city in citymap) {\r\n" + 
					"          var cityCircle = new google.maps.Circle({\r\n" + 
					"            strokeColor: citymap[city].color," + 
					"            strokeOpacity: 0.8,\r\n" + 
					"            strokeWeight: 2,\r\n" + 
					"            fillColor: '#FF0000',\r\n" + 
					"            fillOpacity: 0.35,\r\n" + 
					"            map: map,\r\n" + 
					"            center: citymap[city].center,\r\n" + 
					"            radius: Math.sqrt(citymap[city].population) * 100\r\n" + 
					"          });\r\n" + 
					"        }\r\n");
			print.write("        var lineSymbol = {\r\n" + 
					"           path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW\r\n" + 
					"        };");

			print.write("      }\r\n" + 
					"    </script>\r\n" + 
					"    <script async defer\r\n" + 
					"    src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAKnlQAqpj6hVx7gBRLstQ3PAnpP1LxKjE&callback=initMap\">\r\n" + 
					"    </script>\r\n" + 
					"  </body>\r\n" + 
					"</html>\r\n" + 
					"");
			print.close();
		}catch (Exception e1){e1.printStackTrace();}
		String osName = System.getProperty("os.name");
		try {
			if (osName.startsWith("Windows")) {
				Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath());
			} else if (osName.startsWith("Mac OS X")) {
				// Runtime.getRuntime().exec("open -a safari " + url);
				// Runtime.getRuntime().exec("open " + url + "/index.html");
				Runtime.getRuntime().exec("open " + file.getAbsolutePath());
			} else {
				System.out.println("Please open a browser and go to "+file.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("Failed to start a browser to open the url " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}



	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
		* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

	/**
	 * Carga el grafo a partir del JSon generados en el taller 8
	 * @param serviceFile Sring con la direccion del archivo de carga
	 * @param distancia int con la distancia al rededor de cada vertice
	 * @param tamanio cantidad de datos en el archivo
	 */
	public void req0(String serviceFile,int distancia, String tamanio)
	{
		try 
		{
			BufferedReader bfr = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
			Lista[] lista = gson.fromJson(bfr, Lista[].class);
			Lista listaNodos = (Lista) lista[0];
			Lista listaArcos = (Lista) lista[1];
			grafo = new GrafoDirigido<Double,VerticeItem,InfoArco>();
			this.tamanio = tamanio;
			this.distancia = distancia;
			for(int i = 0 ; i < listaNodos.darLongitud() ; i++)
			{
				LinkedTreeMap<K, V> aux = (LinkedTreeMap<K, V>) listaNodos.darElemento(i);
				String verticeAux = gson.toJson(aux);
				VerticeItem verAux = (VerticeItem) gson.fromJson(verticeAux, VerticeItem.class);
				try 
				{
					grafo.addVertex(verAux.darId(), verAux);
				} 
				catch (VerticeYaExisteException e) {e.printStackTrace();}
			}
			for(int i = 0 ; i < listaArcos.darLongitud() ; i++)
			{
				LinkedTreeMap<K, V> aux = (LinkedTreeMap<K, V>) listaArcos.darElemento(i);
				String verticeAux = aux.toString();
				InfoArco arcoInfo = gson.fromJson(verticeAux, InfoArco.class);
				try 
				{
					grafo.addEdge(arcoInfo.darV1(), arcoInfo.darV2(), arcoInfo);
				} 
				catch (VerticeNoExisteException e){e.printStackTrace();}catch (ArcoYaExisteException e){e.printStackTrace();}
			}
			System.out.println("Vertices: " + grafo.V());
			System.out.println("Arcos: " + grafo.E());
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("No existe el archivo que se desea cargar");
		}
	}
	
	/**
	 * Genera los nuevos puntos de un trayecto dejandolos en los bordes de la representaci�n gr�fica de los vertices.
	 * @param lat1 Latitud del vertice de donde comienza
	 * @param long1 Longitud del vertice de donde comienza
	 * @param lat2 Latitud del vertice donde termina
	 * @param long2 Longitud del vertice donde termina
	 * @param radioSalida Radio del vertice de donde comienza
	 * @param radioLlegada Radio del vertice de donde termina
	 * @return
	 */
	
	public Lista puntosNuevos(double lat1, double long1, double lat2, double long2, int radioSalida, int radioLlegada)
	{
		Lista puntos = new Lista();
		double pendiente = (lat2-lat1)/(long2-long1);
		double theta = Math.atan(pendiente);
		double xPrima = (long2 - long1)/ Math.sqrt(Math.pow(long2-long1, 2)+Math.pow(lat2-lat1, 2));
		double yPrima = (lat2 - lat1)/ Math.sqrt(Math.pow(long2-long1, 2)+Math.pow(lat2-lat1, 2));
		puntos.agregar(lat1 + (((0.001793694*radioSalida)/200)*yPrima));
		puntos.agregar(long1 + (((0.002410327*radioSalida)/200)*xPrima));
		puntos.agregar(lat2 - (((0.001793694*radioLlegada)/200)*yPrima));
		puntos.agregar(long2 - (((0.002410327*radioLlegada)/200)*xPrima));
		return puntos;
	}
	
	/**
	 * Genera los nuevos puntos de un trayecto dejandolos en los bordes de la representaci�n gr�fica de los vertices.
	 * @param lat1 Latitud del vertice de donde comienza
	 * @param long1 Longitud del vertice de donde comienza
	 * @param lat2 Latitud del vertice donde termina
	 * @param long2 Longitud del vertice donde termina
	 */
	public Lista puntosNuevos(double lat1, double long1, double lat2, double long2)
	{
		Lista puntos = new Lista();
		double pendiente = (lat2-lat1)/(long2-long1);
		double theta = Math.atan(pendiente);
		double xPrima = (long2 - long1)/ Math.sqrt(Math.pow(long2-long1, 2)+Math.pow(lat2-lat1, 2));
		double yPrima = (lat2 - lat1)/ Math.sqrt(Math.pow(long2-long1, 2)+Math.pow(lat2-lat1, 2));
		puntos.agregar(lat1 + (0.001793694*yPrima));
		puntos.agregar(long1 + (0.002410327*xPrima));
		puntos.agregar(lat2 - (0.001793694*yPrima));
		puntos.agregar(long2 - (0.002410327*xPrima));
		return puntos;
	}
	
	public Lista sort(Lista pLista, int pTipo)
	{
		int N = pLista.darLongitud();
		Lista nuevaLista = pLista;
		for(int i = 1; i < N; i++) 
		{
			for(int j = i; j-1 >= 0 && less((Camino)nuevaLista.darElemento(j), (Camino)nuevaLista.darElemento(j-1), pTipo); j--)
			{
				exch(nuevaLista, j, j-1);	
			}
		}
		return nuevaLista;
	}
	
	public boolean less(Camino a, Camino b, int pTipo)
	{
		return  a.compareTo(b, pTipo) > 0;
	}
	
	public void exch(Lista pLista, int i , int j)
	{
		Camino t = (Camino) pLista.darElemento(i);
		pLista.asignar(pLista.darElemento(j), i); 
		pLista.asignar(t, j);
	}
}