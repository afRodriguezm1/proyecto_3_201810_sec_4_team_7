package model.data_structures;

import model.vo.InfoArco;
import model.vo.VerticeItem;

public class Kosaraju<K> 
{
	private Lista conectados;
	
	private Lista marked; // reached vertices

	private Lista id; // component identifiers

	private int count; // number of strong components

	public Kosaraju(GrafoDirigido<Double, VerticeItem, InfoArco> G)
	{
		marked = new Lista();
		id = new Lista();
		conectados = new Lista();
		DepthFirstOrder order = new DepthFirstOrder(G.reverse());
		Pila pila = order.reversePost();
		while(!pila.isEmpty())
		{
			Lista aux = new Lista();
			double id = (double) pila.pop();
			if(!isMarked(id))
			{
				dfs(G,id, aux);
				count++;
				conectados.agregar(aux);
			}
		}
	}

	private void dfs(GrafoDirigido<Double, VerticeItem, InfoArco> G, double id, Lista grupoConectado)
	{
		marked.agregar(id);
		grupoConectado.agregar(id);
		try 
		{
				IteradorSimple<Double> it =  G.adj(id);
				while(it.haySiguiente())
				{
					double auxId = (double) it.darSiguiente();
					if(!isMarked(auxId))
						dfs(G, auxId, grupoConectado);
				}
				
			}catch (VerticeNoExisteException e){e.printStackTrace();}
			
	}

	public int count()
	{ 
		return count; 
	}
	
	public Lista grupos()
	{
		return conectados;
	}
	
	public boolean isMarked(double id)
	{
		for(int i = 0; i < marked.darLongitud(); i++)
		{
			if(marked.darElemento(i).equals(id))
				return true;
		}
		return false;
	}
}