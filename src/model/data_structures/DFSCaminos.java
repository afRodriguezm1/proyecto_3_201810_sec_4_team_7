package model.data_structures;


import model.vo.InfoArco;
import model.vo.Item;
import model.vo.VerticeItem;

public class DFSCaminos<K>
{
	private Lista marked;

	private Lista vertices;
	
	private Lista caminos;
	
	private double idInicio;
	
	public DFSCaminos(GrafoDirigido<Double, VerticeItem, InfoArco> G, double idInicio, double idFinal) throws VerticeNoExisteException, ArcoNoExisteException
	{
		marked = new Lista();
		caminos = new Lista();
		this.idInicio = idInicio;
		IteradorSimple<Double> it = G.adj(idInicio);
		Camino<Double,VerticeItem,InfoArco> camino = new Camino<Double,VerticeItem,InfoArco>((VerticeItem)((Vertice)G.darObjetoVertice(idInicio)).darInfoVertice());
		while(it.haySiguiente())
		{
			double id = it.darSiguiente();
			VerticeItem ver = (VerticeItem) ((Vertice)G.darObjetoVertice(id)).darInfoVertice();
			boolean peaje = false;
			Lista listaAux = ver.darItems();
			for(int i = 0; i < listaAux.darLongitud() && peaje == false; i++)
			{
				Item item = (Item) listaAux.darElemento(i);
				if(item.getTolls() != 0)
					peaje = true;
			}
			if(peaje = false)
				continue;
			if(!isInThePath( camino,id) && !isMarked(id))
				dfsNuevo(G, id , camino, idFinal, idInicio);
		}
	}

	private void dfsNuevo(GrafoDirigido<Double, VerticeItem, InfoArco> G, double id, Camino camino, double idFinal, double idAnterior) throws VerticeNoExisteException, ArcoNoExisteException
	{
		marked.agregar(id);

		Camino<Double,VerticeItem,InfoArco> nuevoCamino = camino;
		nuevoCamino.agregarArcoFinal((VerticeItem)((Vertice)G.darObjetoVertice(id)).darInfoVertice(), (InfoArco)((Arco)G.getInfoArc(idAnterior, id)).darInfoArco());
		if(id == idFinal)
		{
			nuevoCamino.agregarArcoFinal((VerticeItem)((Vertice)G.darObjetoVertice(id)).darInfoVertice(), (InfoArco)((Arco)G.getInfoArc(idAnterior, id)).darInfoArco());
			caminos.agregar(nuevoCamino);
			desmarcar(id);
		}
		else
		{
			try 
			{
				IteradorSimple<Double> it =  G.adj(id);
				while(it.haySiguiente())
				{
					double auxId = (double) it.darSiguiente();
					VerticeItem ver = (VerticeItem) ((Vertice)G.darObjetoVertice(auxId)).darInfoVertice();
					boolean peaje = false;
					Lista listaAux = ver.darItems();
					for(int i = 0; i < listaAux.darLongitud(); i++)
					{
						Item item = (Item) listaAux.darElemento(i);
						if(item.getTolls() != 0)
							peaje = true;
					}
					if(peaje = true)
						continue;
					if(!isInThePath(nuevoCamino,auxId) && !isMarked(auxId))
					{
						dfsNuevo(G, auxId, nuevoCamino, idFinal, id);
					}
				}
				
			}catch (VerticeNoExisteException e){e.printStackTrace();}
		}
	}

	public boolean isMarked(double id)
	{
		for(int i = 0; i < marked.darLongitud(); i++)
		{
			if(marked.darElemento(i).equals(id))
				return true;
		}
		return false;
	}
	
	public void desmarcar(double id)
	{
		for(int i = 0; i < marked.darLongitud(); i++)
		{
			if(marked.darElemento(i).equals(id))
				marked.eliminar(i);
		}
	}
	
	public Lista darCaminos()
	{
		return caminos;
	}
	
	public boolean isInThePath(Camino<Double,VerticeItem,InfoArco> camino, double id)
	{
		Lista verticesCamino = camino.darVertices();
		for(int i = 0; i < verticesCamino.darLongitud(); i++)
		{
			if(((VerticeItem)verticesCamino.darElemento(i)).darId().equals(id))
				return true;
		}
		return false;
	}
}
