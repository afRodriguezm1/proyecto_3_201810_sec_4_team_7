package model.data_structures;


import model.vo.InfoArco;
import model.vo.VerticeItem;

public class DepthFirstOrder<K>
{
	/**
	 * Lista con los vertices ya visitados
	 */
	private Lista marked;

	/**
	 * Lista con los vertices
	 */
	private Lista vertices;

	/**
	 * Pila con la que se hace el posOrden
	 */
	private Pila<Integer> reversePost; // vertices in reverse postorder

	public DepthFirstOrder(GrafoDirigido<Double, VerticeItem, InfoArco> G)
	{
		marked = new Lista();
		reversePost = new Pila<>();
		vertices = G.darVertices();
		for(int i = 0; i < vertices.darLongitud(); i++)
		{
			if(!isMarked(((VerticeItem)vertices.darElemento(i)).darId()))
				dfs(G, ((VerticeItem)vertices.darElemento(i)).darId());			
		}
	}

	private void dfs(GrafoDirigido<Double, VerticeItem, InfoArco> G, double id )
	{
		marked.agregar(id);
		try 
		{
			IteradorSimple<Double> it =  G.adj(id);
			while(it.haySiguiente())
			{
				double auxId = (double) it.darSiguiente();
				if(!isMarked(auxId))
					dfs(G, auxId);
			}
			
		}catch (VerticeNoExisteException e){e.printStackTrace();}
		reversePost.push(id);
	}

	public Pila reversePost()
	{ 
		return reversePost; 
	}

	public boolean isMarked(double id)
	{
		for(int i = 0; i < marked.darLongitud(); i++)
		{
			if(marked.darElemento(i).equals(id))
				return true;
		}
		return false;
	}
}
