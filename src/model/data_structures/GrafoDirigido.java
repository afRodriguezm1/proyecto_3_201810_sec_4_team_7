/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: GrafoDirigido.java,v 1.3 2008/10/12 04:50:53 alf-mora Exp $
 * Universidad de los Andes (Bogot� - Colombia)
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 * Licenciado bajo el esquema Academic Free License version 2.1 
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co)
 * Framework: Cupi2Collections
 * Autor: Pablo Barvo - Mar 28, 2006
 * Autor: J. Villalobos - Abr 14, 2006
 * Autor: Juan Erasmo Gomez - Ene 28, 2008
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package model.data_structures;

import java.io.Serializable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;

import model.vo.InfoArco;
import model.vo.VerticeItem;


/**
 * Representa un grafo dirigido
 * 
 * @param <K> Tipo del identificador de un v�rtice
 * @param <V> Tipo de datos del elemento del v�rtice
 * @param <A> Tipo de datos del elemento del arco
 */
public class GrafoDirigido<K, V extends IVertice<K>, A extends IArco> implements Serializable
{
	// -----------------------------------------------------------------
	// Constantes
	// -----------------------------------------------------------------

	/**
	 * Constante para la serializaci�n
	 */
	private static final long serialVersionUID = 1L;

	// -----------------------------------------------------------------
	// Constantes
	// ------------------------------------------------------------------
	/**
	 * Constante que representa un valor infinito
	 */
	public static final int INFINITO = -1;

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * Tabla de hashing con los v�rtices
	 */
	private HashMap<K, Vertice<K, V, A>> vertices;

	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	/**
	 * Crea un nuevo grafo vac�o
	 */
	public GrafoDirigido( )
	{
		vertices = new HashMap<K, Vertice<K, V, A>>( );
	}

    /**
     * Retorna el n�mero de arcos que tiene el grafo
     * @return el n�mero de arcos que tiene el grafo
     */
    public int E( )
    {
        return darArcos( ).darLongitud( );
    }
    
    public int V()
    {
    	return vertices.size();
    }
    
    /**
     * Borra las marcas de todos los v�rtices del grafo
     */
    private void reiniciarMarcas( )
    {
        // Elimina todas las marcas presentes en los v�rtices del grafo
        for( Vertice<K, V, A> vertice : vertices.values( ) )
        {
            vertice.desmarcar( );
        }
    }

    
    /**
     * Calcula todos los caminos m�nimos desde el v�rtice dado hacia los dem�s v�rtices del grafo
     * @param idVertice El identificador del v�rtice
     * @return Los caminos m�nimos desde el v�rtice especificado hac�a los dem�s nodos
     * @throws VerticeNoExisteException Si el v�rtice especificado no existe
     */
    public CaminosMinimos<K, V, A> dijkstra( K idVertice ) throws VerticeNoExisteException
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene el v�rtice
        Vertice<K, V, A> vertice = darObjetoVertice( idVertice );
        // Inicializa la estructura que va a permitir representar los caminos
        // m�nimos que van
        // desde v�rtice dado a todos los dem�s v�rtices del grafo
        CaminosMinimos<K, V, A> minimos = new CaminosMinimos<K, V, A>( vertice, darObjetosVertices( ) );
        return vertice.dijkstra( minimos );
    }
    
    /**
     * Retorna el camino m�s barato (de menor costo) entre el par de v�rtices especificados
     * @param idVerticeOrigen V�rtice en el que inicia el camino
     * @param idVerticeDestino V�rtice en el que termina el camino
     * @return El camino m�s barato entre el par de v�rtices especificados
     * @throws VerticeNoExisteException Si alguno de los dos v�rtices no existe
     */
    public Camino<K, V, A> darCaminoMasBarato( K idVerticeOrigen, K idVerticeDestino ) throws VerticeNoExisteException
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene los v�rtices
        Vertice<K, V, A> verticeOrigen = darObjetoVertice( idVerticeOrigen );
        Vertice<K, V, A> verticeDestino = darObjetoVertice( idVerticeDestino );
        // Le pide al v�rtice de origen que localice el camino
        return verticeOrigen.darCaminoMasBarato( verticeDestino );
    }
    
    public Camino<K, V, A> darCaminoMasBarato2( K idVerticeOrigen, K idVerticeDestino ) throws VerticeNoExisteException
    {
        // Borra todas las marcas presentes en el grafo
        reiniciarMarcas( );
        // Obtiene los v�rtices
        Vertice<K, V, A> verticeOrigen = darObjetoVertice( idVerticeOrigen );
        Vertice<K, V, A> verticeDestino = darObjetoVertice( idVerticeDestino );
        // Le pide al v�rtice de origen que localice el camino
        return verticeOrigen.darCaminoMasBarato2( verticeDestino );
    }
    
    /**
     * Devuelve todos los v�rtices del grafo.
     * </p>
     * Este m�todo retorna los objetos Vertice, propios de est� implementaci�n.
     * @return Vertices del grafo
     */
    public Collection<Vertice<K, V, A>> darObjetosVertices( )
    {
        return vertices.values( );
    }
    
	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------


	/**
	 * Devuelve todos los arcos del grafo
	 * @return Arcos del grafo
	 */
	public Lista<A> darArcos( )
	{
		Lista<A> arcos = new Lista<A>( );

		// Recorre todos los v�rtices buscando los arcos
		for( Vertice<K, V, A> vertice : vertices.values( ) )
		{
			// Recorrer los arcos del v�rtice y poblar la lista arcos
			for( Arco<K, V, A> arco : vertice.darSucesores( ) )
				arcos.agregar( arco.darInfoArco( ) );
		}
		return arcos;

	}

	/**
	 * Indica si el v�rtice con el identificador dado existe en el grafo
	 * @param idVertice Identificador del v�rtice
	 * @return <code>true</code> si el v�rtice con el identificador dado existe o <code>false</code> en caso contrario
	 */
	private boolean existeVertice( Object idVertice )
	{
		return vertices.get( idVertice ) != null;
	}
	/**
	 * Devuelve el v�rtice identificado con el identificador especificado
	 * @param idVertice Identificador del v�rtice
	 * @return V�rtice buscado
	 * @throws VerticeNoExisteException Excepci�n generada cuando el v�rtice buscado no existe en el grafo
	 */
	
	public Vertice<K, V, A> darObjetoVertice( Object idVertice ) throws VerticeNoExisteException
	{
		Vertice<K, V, A> vertice = vertices.get( idVertice );
		if( vertice == null )
		{
			throw new VerticeNoExisteException( "El v�rtice buscado no existe en el grafo", idVertice );
		}
		return vertice;
	}

	/**
	 * Crea un nuevo v�rtice en el grafo
	 * @param elemento Elemento del v�rtice
	 * @throws VerticeYaExisteException Si el v�rtice que se quiere agregar ya existe
	 */
	public void addVertex(K id, V elemento ) throws VerticeYaExisteException
	{
		if( existeVertice( id ) )
		{}
		else
		{
			Vertice<K, V, A> vertice = new Vertice<K, V, A>( elemento );
			vertices.put( id, vertice );
		}
	}

	/**
	 * Devuelve el v�rtice identificado con el identificador especificado
	 * @param idVertice Identificador del v�rtice
	 * @return V�rtice buscado
	 * @throws VerticeNoExisteException Excepci�n generada cuando el v�rtice buscado no existe en el grafo
	 */
	public V getInfoVertex( K idVertice ) throws VerticeNoExisteException
	{
		V vertice = vertices.get( idVertice ).darInfoVertice();
		if( vertice == null )
		{
			throw new VerticeNoExisteException( "El v�rtice buscado no existe en el grafo", idVertice );
		}
		return vertice;
	}

	public void setInfoVertex(K idVertex, V infoVertex)throws Exception
	{
		Vertice<K, V, A> vertice = vertices.get(idVertex);
		if(vertice == null)
		{
			throw new VerticeNoExisteException( "El v�rtice buscado no existe en el grafo", idVertex );
		}
		else
		{
			vertice.cambiarInformacion(infoVertex);
		}
	}

	/**
	 * Agrega un nuevo arco al grafo
	 * @param idVerticeOrigen Identificador del v�rtice desde donde sale el arco
	 * @param idVerticeDestino Identificador del v�rtice hasta donde llega el arco
	 * @param infoArco Elemento del arco
	 * @throws VerticeNoExisteException Si alguno de los v�rtices especificados no existe
	 * @throws ArcoYaExisteException Si ya existe un arco entre esos dos v�rtices
	 */
	public void addEdge( Object idVerticeOrigen, K idVerticeDestino, A infoArco ) throws VerticeNoExisteException, ArcoYaExisteException
	{
		// Obtiene los v�rtices
		Vertice<K, V, A> verticeOrigen = darObjetoVertice( idVerticeOrigen );
		Vertice<K, V, A> verticeDestino = darObjetoVertice( idVerticeDestino );
		// Crea el arco y lo agrega
		Arco<K, V, A> arco = new Arco<K, V, A>( verticeOrigen, verticeDestino, infoArco );
		verticeOrigen.agregarArco( arco );
	}



	/**
	 * Retorna los v�rtices del grafo.
	 * @return Los v�rtices del grafo.
	 */
	public Lista<V> darVertices( )
	{
		// Crear la lista
		Lista<V> vs = new Lista<V>( );

		// Recorrer los v�rtices y poblar la lista
		for( Vertice<K, V, A> v : vertices.values( ) )
		{
			vs.agregar( v.darInfoVertice( ) );
		}

		// Retornar la lista
		return vs;
	}
	/**
	 * Retorna el arco entre los v�rtices ingresados por parametros
	 * @param idV1 id del primer v�rtice
	 * @param idV2 id del segundo v�rtice
	 * @return El arco entre los v�rtices ingresados por parametros
	 * @throws VerticeNoExisteException si alguno de los v�rtices ingresados por parametros no existe en el grafo
	 * @throws ArcoNoExisteException si no existe un arco entre esos v�rtices
	 */
	public Arco getInfoArc( Object idVerticeOrigen, Object idVerticeDestino ) throws VerticeNoExisteException, ArcoNoExisteException
	{
		// Busca el primer v�rtice y luego busca el arco
		Vertice<K, V, A> vertice = darObjetoVertice( idVerticeOrigen );
		if( existeVertice( idVerticeDestino ) )
		{
			Arco<K, V, A> arco = vertice.darArco( idVerticeDestino );
			if( arco == null )
				throw new ArcoNoExisteException( "No existe un arco entre los v�rtices seleccionados", idVerticeOrigen, idVerticeDestino );
			else
				return arco;
		}
		else
			throw new VerticeNoExisteException( "V�rtice destino no existe", idVerticeDestino );
	}

	public void setInfoArc(K idVerticeOrigen, K idVerticeDestino, A infoArco) throws VerticeNoExisteException, ArcoNoExisteException
	{
		Arco arco =  getInfoArc(idVerticeOrigen, idVerticeDestino);
		arco.ponerInfo(infoArco);
	}


	/**
	 * Devuelve los id de los v�rtice sucedores a un v�rtice ingresado por par�metro
	 * @param v Identificador del v�rtice
	 * @return Los id de los v�rtice sucedores a un v�rtice ingresado por par�metro
	 * @throws VerticeNoExisteException Si el v�rtice especificado no existe
	 */
	public IteradorSimple<K> adj( double v ) throws VerticeNoExisteException
	{
		Lista<K> lista = new Lista<K>( );
		for( Arco<K, V, A> a : darObjetoVertice( v ).darSucesores( ) )
		{
			lista.agregar( a.darVerticeDestino( ).darId() );
		}
		return (IteradorSimple<K>) lista.darIterador();
	}
	
	public GrafoDirigido<Double, VerticeItem, IArco> reverse()
	{
		GrafoDirigido<Double, VerticeItem, IArco> nuevo = new GrafoDirigido<>();
		Lista vertices = this.darVertices();
		VerticeItem verticeA = null;
		for(int i = 0; i < vertices.darLongitud(); i++)
		{
			try 
			{
				verticeA = (VerticeItem) vertices.darElemento(i);
				nuevo.addVertex(verticeA.darId(), verticeA);
			} 
			catch(VerticeYaExisteException e){e.printStackTrace();}
		}
		for(int i = 0; i < vertices.darLongitud(); i++)
		{
			verticeA = (VerticeItem) vertices.darElemento(i);
			Vertice<K, IVertice<K>, IArco> vertice = null;
			try 
			{
				 vertice = (Vertice<K, IVertice<K>, IArco>) darObjetoVertice( verticeA.darId());
			} catch (VerticeNoExisteException e1) { e1.printStackTrace();}
			ArrayList arcos = vertice.darSucesores();
			
			for(int j = 0; j<arcos.size(); j++)
			{
				try 
				{
					nuevo.addEdge(((Arco)arcos.get(j)).darVerticeDestino().darId(), verticeA.darId(), ((Arco)arcos.get(j)).darInfoArco());
				} 
				catch (Exception e) {e.printStackTrace();}
			}
		}
		return nuevo;
	}
}
