package model.data_structures;

public class Cola<T>
{
	/**
	 * Nodo en la primera posicion en la cola.
	 */
	private Node<T> first;
	
	/**
	 * Nodo en la ultima posicion en la cola.
	 */
	private Node<T> last;
	
	/**
	 * Tama�o de la cola.
	 */
	private int size;
	
	/**
	 * Agrega un nodo a la cola, agregandolo al final de la cola.
	 */
	public void enqueue(T item) 
	{
		Node<T> newNodo = new Node(item);
		if(size==0)
		{
			first = newNodo;
			last = newNodo;
		}
		else
		{
			Node<T> oldLast = last;
			oldLast.setNextNode(newNodo);
			last = newNodo;
		}
		size++;
	}

	public T dequeue() 
	{
		if(size == 0)
			return null;
		Node<T> node = first;
		first = node.getNext();
		node.setNextNode(null);
		size--;
		return node.getItem();
	}

	public int getSize() 
	{
		return size;
	}

	public boolean isEmpty() 
	{
		return (size == 0)?true : false;
	}

}
