package model.data_structures;

public class Pila<T>
{
	/**
	 * Cabeza de la pila.
	 */
	private Node topStack;
	
	/**
	 * Tama�o de la pila.
	 */
	private int size;
	
	/**
	 * Agrega un nodo al principio de la pila.
	 * @param Node que se agrega al principio de la pila.
	 */
	public void push(double id) 
	{
		Node<T> newNode = new Node(id);
		if(topStack==null)
			topStack = newNode;
		else
		{
			newNode.setNextNode(topStack);
			topStack = newNode;
		}
		size++;
	}

	/**
	 * Retorna el ultimo nodo agregado a la pila.
	 * @return Node del principio de la pila.
	 */
	public T pop() 
	{
		if(topStack==null)
			return null;
		Node nodo = topStack;
		topStack = topStack.getNext();
		size--;
		return (T) nodo.getItem();
	}

	public int getSize() 
	{
		return size;
	}

	/**
	 * Verifica si esta vacia la pila.
	 * @return true si esta vacia, false de lo contrario.
	 */
	public boolean isEmpty() 
	{
		return (size != 0)? false : true;
	}

}
